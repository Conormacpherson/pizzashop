$(document).ready(function() {
    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {request : "toppings"},
        success : function(response) {
            
            var total = 10.0;

            if(response["toppings"] != "empty") {

                jQuery.each(response, function(index, element) {
                    var responseArray = response[index];
                    jQuery.each(responseArray, function(responseIndex, responseElement) {
                        var price = 1.0;
                        if(responseArray[responseIndex] == "Cheese") {
                            price = 1.5;
                            total += 1.5;
                        }
                        else {
                            price = 1.0;
                            total += 1.0;
                        }
                        $("#pizzaToppings").append("<tr><td>" + responseArray[responseIndex] + "</td><td>$" + price.toFixed(2) +"</td></tr>");
                    });
                });
            }
            $("#totalTag").text("Total: $" + total.toFixed(2));
        }
    });
});

$("#confirmOrder").on("click", function() {
    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {orderStatus : "confirm"},
        success : function(response) {

            if(response["orderStatus"] == "confirmRecieved") {

                location.href="pizzaPage4.html";
            }
        }
    });
});

 $("#cancelOrder").on("click", function() {
    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {orderStatus : "cancel"},
        success : function(response) {

            if(response["orderStatus"] == "cancelRecieved") {

                location.href="pizzaPage4.html";
            }
        }
    });
});