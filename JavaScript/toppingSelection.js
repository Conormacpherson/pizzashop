$(document).ready(function() {

    /// When the "Make It!" button is pushed, submit the selections to the next page through Jquery/AJAX 
    $("#submitToppings").on("click", function() {
        
        var selectedToppings = [];
        $.each($("input[name='topping']:checked"), function() {
            selectedToppings.push($(this).val());    
        });

        if(selectedToppings === undefined || selectedToppings.length == 0) {

            selectedToppings = "empty";
        }

        var json_text = JSON.stringify(selectedToppings);
        $.ajax({
            url : 'php/pizzaShop.php',
            type : 'POST',
            dataType : 'json',
            data : {toppings : json_text},
            success : function(response) {

                if(response["message"] == "toppingsRecieved") {

                    location.href="pizzaPage3.html";
                }
            }
        });
    });
    

     var isChecked = $('#t1').prop('checked');
    if(isChecked)
    {
        document.getElementById('pep').style.display="block";
    }
    else
    {
        document.getElementById('pep').style.display="none";
    } 

    var isChecked = $('#t2').prop('checked');
    if(isChecked)
    {
        document.getElementById('mush').style.display="block";
    }
    else
    {
        document.getElementById('mush').style.display="none";
    } 

    var isChecked = $('#t3').prop('checked');
    if(isChecked)
    {
        document.getElementById('golive').style.display="block";
    }
    else
    {
        document.getElementById('golive').style.display="none";
    } 
    var isChecked = $('#t4').prop('checked');
    if(isChecked)
    {
        document.getElementById('gpep').style.display="block";
    }
    else
    {
        document.getElementById('gpep').style.display="none";
    } 

    //If the user selects a topping, place the topping onto the pizza that is displayed on screen.
    $("#t1").change(function(){
        var isChecked = $('#t1').prop('checked');
        if(isChecked)
        {
            document.getElementById('pep').style.display="block";
        }
        else
        {
            document.getElementById('pep').style.display="none";
        } 
    });

    $("#t2").change(function(){
        var isChecked = $('#t2').prop('checked');
        if(isChecked)
        {
            document.getElementById('mush').style.display="block";
        }
        else
        {
            document.getElementById('mush').style.display="none";
        } 
    });

    $("#t3").change(function(){
        var isChecked = $('#t3').prop('checked');
        if(isChecked)
        {
            document.getElementById('golive').style.display="block";
        }
        else
        {
            document.getElementById('golive').style.display="none";
        } 
    });

    $("#t4").change(function(){
        var isChecked = $('#t4').prop('checked');
        if(isChecked)
        {
            document.getElementById('gpep').style.display="block";
        }
        else
        {
            document.getElementById('gpep').style.display="none";
        } 
    });
    // End placing toppings onto pizza

    // Check box price calculation using Jquery/AJAX
    $("input[name='topping']").change(function() {

        var selectedToppings = [];
        $.each($("input[name='topping']:checked"), function() {
            selectedToppings.push($(this).val());    
        });

        if(selectedToppings === undefined || selectedToppings.length == 0) {

            selectedToppings = "empty";
        }

        $.ajax({
            url : 'php/pizzaShop.php',
            type : 'POST',
            dataType : 'json',
            data : {currentToppings : selectedToppings},
            success : function(response) {

                $("#currentCost").text("Current Cost: $" + parseFloat(response["runningTotal"]).toFixed(2));
            }
        });
    });
});