// Load the first name to the page (from page 1)
$(document).ready(function() {

    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {request : "firstName"},
        success : function(response) {
            $(".greeting").text("Ciao " + response["name"]);
        }
    });
});