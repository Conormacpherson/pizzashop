$(document).ready(function() {
    
    var message = "";

    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {request : "firstName"},
        async : false,
        success : function(response) {
            message += response["name"];
        }
    });

    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        async : false,
        data : {request : "lastName"},
        success : function(response) {

            if(response["name"] !== null) {
                
                message += " " + response["name"];
            }
        }
    });

    $.ajax({
        url : 'php/pizzaShop.php',
        type : 'POST',
        dataType : 'json',
        data : {request : "orderStatus"},
        success : function(response) {

            if(response["orderStatus"] == "confirm") {

                message += ", thank you for confirming your order from the SET Pizza Shop!<br />"
                                + "Have a good day! Your pizza will arrive within the hour.";

                $(".hello").html(message);
            }
            else if(response["orderStatus"] == "cancel") {
                
                message += ", Sorry to hear that you cancelled your order...Maybe we'll get it right next time.";

                $(".hello").text(message);
            }
        }
    });
});