function validateName() {
    
    var name = document.getElementById("nameInput").value;
    if(/^[a-zA-Z ]+$/.test(name)) {
        return true;
    }

    document.getElementById("error").style.display = "block";
    return false;
}