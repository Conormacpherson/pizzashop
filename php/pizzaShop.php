<?php
    session_start();

    // If the user is submitting the name, add the name to the session and send a name recieved response.
    if($_POST["name"] != null) {

        $name = $_POST["name"];
        $names = explode(" ", $name);

        if($names != null) { 

            $_SESSION["firstName"] = $names[0];
            $_SESSION["lastName"] = $names[1];
            $response["nameResponse"] = "nameRecieved";
            echo json_encode($response);
        }
        else {

            $_SESSION["firstName"] = $name;
            $response["nameResponse"] = "nameRecieved";
            echo json_encode($response["nameResponse"]);
        }
    }


    // If the second page is requesting the name from the server, get the name from the session and
    // send it in a response.
    if($_POST["request"] == "firstName") {

        $response["name"] = $_SESSION["firstName"];
        echo json_encode($response);
    }

    if($_POST["request"] == "lastName") {

        $response["name"] = $_SESSION["lastName"];
        echo json_encode($response);
    }


    // If the user is selecting or deselecting the toppings, update the running total.
    if($_POST["currentToppings"] != null) {

        $runningTotal = 10.0;

        $toppings = $_POST["currentToppings"];

        foreach ($toppings as $topping) {

            if($topping === "Cheese") {

                $runningTotal += 1.50;
            }
            else {

                $runningTotal += 1.0;
            }
        }

        $response["runningTotal"] = $runningTotal;
        echo json_encode($response);
    }
    else if($_POST["currentToppings"] == "empty") {

        $runningTotal = 10.0;
        $response["runningTotal"] = $runningTotal;
        echo json_encode($response);
    }


    // If the user is submitting their toppings, add the toppings array to the session
    // and send a toppings recieved response.
    if($_POST["toppings"] != null) {

        $_SESSION["toppings"] = json_decode($_POST["toppings"]);
        $response["message"] = "toppingsRecieved";
        echo json_encode($response);
    }
    else if($_POST["toppings"] == "empty") {

        $_SESSION["toppings"] = null;
        $response["message"] = "toppingsRecieved";
        echo json_encode($response);
    }

    if($_POST["orderStatus"] != null) {

        $_SESSION["orderStatus"] = $_POST["orderStatus"];
        if($_SESSION["orderStatus"] == "confirm") {

            $response["orderStatus"] = "confirmRecieved";
            echo json_encode($response);
        }
        else if($_SESSION["orderStatus"] == "cancel") {

            $response["orderStatus"] = "cancelRecieved";
            echo json_encode($response);
        }
    }

    if($_POST["request"] == "orderStatus") {

        $response["orderStatus"] = $_SESSION["orderStatus"];
        echo json_encode($response);
    }

    if($_POST["request"] == "toppings") {

        $response["toppings"] = $_SESSION["toppings"];
        echo json_encode($response);
    }
?>